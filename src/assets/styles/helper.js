import { css } from 'lit-element';

export default css `
  .swb-flex {
    display: flex;
  }

  .swb-flex-wrap {
    display: flex;
    flex-wrap: wrap;
  }

  .swb-flex-grow-1 {
    flex-grow: 1;
  }

  .swb-align-center {
    align-items: center;
  }

  .swb-justify-between {
    justify-content: space-between;
  }

  .swb-justify-end {
    justify-content: flex-end;
  }

  .swb-no-wrap {
    white-space: nowrap;
  }

  .swb-cta-group {
    display: flex;
    justify-content: flex-end;
  }

  .swb-cta-group > :not(:last-child) {
    margin-right: var(--side-spacing-small);
  }

  .swb-card-grid {
    display: grid;
    grid-auto-flow: column;
    grid-gap: var(--side-spacing-regular);
    grid-template-columns: 1fr 1fr 1fr;
  }

  @media only screen and (min-width: 1024px) {
    .m-r-lg-20 {
      margin-right: 20px;
    }

    .mb-lg-10 {
      margin-bottom: 10px;
    }

    .hidden-lg {
      display: none;
    }
  }

  @media only screen and (max-width: 1024px) {
    .hidden-md {
      display: none;
    }

    .swb-cta-group {
      margin-top: var(--side-spacing-large);
      flex-direction: column;
    }

    .swb-cta-group > :not(:last-child) {
      margin: var(--side-spacing-small) 0;
    }

    .swb-card-grid {
      grid-auto-flow: row;
      grid-gap: var(--side-spacing-small);
      grid-template: none;
    }
  }
`;