import { css } from 'lit-element';

export default css `
blockquote,
dd,
dl,
fieldset,
figure,
h1,
h2,
h3,
h4,
h5,
hr,
legend,
p,
pre,
button,
ul {
  margin: 0;
  padding: 0;
  list-style: none;
}
h1 {
  font-size: 36px;
}
h2 {
  font-size: 20px;
}
h3 {
  font-size: 18px;
}
h4 {
  font-size: 15px;
}
h5 {
  font-size: 13px;
}
img {
  max-width: 100%;
}
a {
  text-decoration: none;
}
input {
  -webkit-font-smoothing: antialiased;
  -moz-osx-font-smoothing: grayscale;
  color: var(--text-color-regular);
  font-family: var(--font-family-regular);
  font-size: var(--fs-regular);
  line-height: var(--lh-regular);
  outline: none;
  outline-offset: unset;
}
* {
  box-sizing: border-box;
}
`;