export default {
  data: {
    almond: '--color-almond',
    apricot: '--color-apricot',
    blue: '--color-blue',
    brown: '--color-brown',
    gray: '--color-light-gray',
    pink: '--color-pink',
    orange: '--color-orange-main',
    turquoise: '--color-turquoise-dark',
    wafer: '--color-wafer',
    white: '--color-white',
    wood: '--color-light-wood',
    yellow: '--color-yellow',
  }
}