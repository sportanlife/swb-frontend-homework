import {css} from "lit-element";

export default css`
  .swb-form-row {
    display: flex;
  }

  .swb-input-group {
    display: flex;
    width: 65%;
    max-width: 350px;
  }

  .swb-form-row:not(:last-of-type) {
    margin-bottom: var(--side-spacing-small);
  }

  .swb-input-group :not(:last-child) {
    margin-right: var(--side-spacing-small);
  }

  @media only screen and (max-width: 1024px) {
    .swb-form-row {
      flex-direction: column;
    }

    .swb-form-row:not(:last-of-type) {
      margin-bottom: 0;
    }

    .swb-input-group {
      flex-direction: column;
      max-width: 100%;
      width: 100%;
    }

    .swb-input-group :not(:last-child) {
      margin: 0 0 var(--side-spacing-small);
    }
  }

`;
