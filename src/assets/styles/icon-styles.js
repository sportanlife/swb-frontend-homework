import {css} from "lit-element";

export const iconStyles = css `
  @font-face {
    font-family: 'icomoon';
    src: url('../fonts/icomoon/fonts/icomoon.eot?qg4fdi');
    src: url('../fonts/icomoon/fonts/icomoon.eot?qg4fdi#iefix') format('embedded-opentype'),
    url('../fonts/icomoon/fonts/icomoon.ttf?qg4fdi') format('truetype'),
    url('../fonts/icomoon/fonts/icomoon.woff?qg4fdi') format('woff'),
    url('../fonts/icomoon/fonts/icomoon.svg?qg4fdi#icomoon') format('svg');
    font-weight: normal;
    font-style: normal;
    font-display: block;
  }

  [class^="icon-"], [class*=" icon-"] {
    /* use !important to prevent issues with browser extensions that change fonts */
    font-family: 'icomoon' !important;
    speak: never;
    font-style: normal;
    font-weight: normal;
    font-variant: normal;
    text-transform: none;
    line-height: 1;

    /* Better Font Rendering =========== */
    -webkit-font-smoothing: antialiased;
    -moz-osx-font-smoothing: grayscale;
  }

  .icon-home:before {
    content: "\\e900";
  }

  .icon-pdf:before {
    content: "\\e901";
    color: rgb(49, 163, 174);
  }

  .icon-pdf:after {
    content: "\\e902";
    margin-left: -1em;
    color: rgb(255, 255, 255);
  }

  .icon-wallet:before {
    content: "\\e903";
  }

  .icon-facebook:before {
    content: "\\e904";
  }

  .icon-instagram:before {
    content: "\\e905";
    color: rgb(81, 43, 43);
  }

  .icon-instagram:after {
    content: "\\e906";
    margin-left: -1em;
    color: rgb(251, 242, 234);
  }

  .icon-skype:before {
    content: "\\e907";
  }

  .icon-twitter:before {
    content: "\\e908";
  }

  .icon-youtube:before {
    content: "\\e909";
  }

`;