export default {
  data: [
    {
      name: 'Home',
      value: 'home',
      icon: 'home'
    },
    {
      name: 'Everyday banking',
      value: 'banking',
      icon: 'wallet'
    }
  ]
}