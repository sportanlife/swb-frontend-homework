export default {
  account: [
    {
      id: 0,
      value: 'Account1',
      name: 'Account name 1'
    },
    {
      id: 1,
      value: 'Account2',
      name: 'Account name 2'
    },
    {
      id: 2,
      value: 'Account3',
      name: 'Account name 3'
    },
    {
      id: 3,
      value: 'Account4',
      name: 'Account name 4'
    }
  ],
  savedPayments: [
    {
      id: 0,
      value: 'Saved payment 1',
      name: 'Saved payment 1'
    },
    {
      id: 1,
      value: 'Saved payment 2',
      name: 'Saved payment 2'
    },
    {
      id: 2,
      value: 'Saved payment 3',
      name: 'Saved payment 3'
    },
    {
      id: 3,
      value: 'Saved payment 4',
      name: 'Saved payment 4'
    }
  ],
  currency: [
    {
      id: 0,
      value: 'eur',
      name: 'EUR'
    },
    {
      id: 1,
      value: 'usd',
      name: 'USD'
    }
  ]

}