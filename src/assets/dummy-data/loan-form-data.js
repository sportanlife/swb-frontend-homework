export default {
  loan: {
    min: '32000',
    max: '320000',
    defaultValue: '32000'
  },
  period: [
    {
      id: 0,
      value: '10',
      name: '10 years'
    },
    {
      id: 1,
      value: '20',
      name: '20 years'
    },
    {
      id: 2,
      value: '30',
      name: '30 years'
    },
    {
      id: 3,
      value: '40',
      name: '40 years'
    }
  ],
  interest: [
    {
      id: 0,
      value: '1',
      name: '1 %'
    },
    {
      id: 1,
      value: '2.3',
      name: '2.3 %'
    },
    {
      id: 2,
      value: '3',
      name: '3 %'
    },
    {
      id: 3,
      value: '4.5',
      name: '4.5 %'
    }
  ],
  currency: [
    {
      id: 0,
      value: 'EUR'
    },
    {
      id: 1,
      value: 'USD'
    }
  ]

}