export default {
  rows: [
    {
      accountNumber: 'EE752200221057734534',
      available: '900.56',
      balance: '3 342 000.00',
      credit: '20.00',
      name: 'Siim Tamm',
      reserved: '725.00'
    },
    {
      accountNumber: 'EE752200221057734534',
      available: '3000',
      balance: '50.90',
      credit: '2000.00',
      name: 'Marju Lepik',
      reserved: null,
    },
    {
      accountNumber: 'EE752200221057734534',
      available: '900.56',
      balance: '12 041.00',
      credit: '20.00',
      name: 'Liina Roosipõld',
      reserved: null
    }
  ]
}