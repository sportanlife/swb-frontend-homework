export default {
  icons: [
    {name: 'facebook', url: 'https://www.facebook.com'},
    {name: 'instagram', url: 'https://www.instagram.com'},
    {name: 'youtube', url: 'https://www.youtube.com'},
    {name: 'twitter', url: 'https://www.twitter.com'},
    {name: 'skype', url: 'https://www.skype.com'}
  ]
}