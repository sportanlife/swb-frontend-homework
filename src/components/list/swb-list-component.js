import {LitElement, html, css} from 'lit-element';
import resetCSS from '../../assets/styles/reset-css';

/**
 * @tag swb-list.
 * @Prop color - List decorator color.
 * @Slot swb-list-item component.
 */
class SwbListComponent extends LitElement {
  static get styles() {
    return [
      resetCSS,
      css`
        :host {
          display: block;
        }

        :host ::slotted(swb-list-item) {
          align-items: start;
          display: flex;
          padding-left: var(--side-spacing-small);
          margin-bottom: 5px;
        }

        :host ::slotted(swb-list-item):before {
          content: '';
          border-bottom: 8px solid var(--color-pink);
          border-left: 8px solid transparent;
          border-radius: 50%;
          border-right: 8px solid var(--color-pink);
          border-top: 8px solid transparent;
          font-size: var(--fs-small);
          line-height: var(--lh-regular);
          margin: 2px 5px 0 -16px;
          transform: rotate(-45deg);
        }

        :host([color='yellow']) ::slotted(swb-list-item):before {
          border-bottom-color: var(--color-yellow);
          border-right-color: var(--color-yellow);
        }

        :host([color='blue']) ::slotted(swb-list-item):before {
          border-bottom-color: var(--color-blue);
          border-right-color: var(--color-blue);
        }
      `
    ];
  }

  static get properties() {
    return {
      color: {type: String}
    };
  }

  constructor() {
    super();

    this.color = 'pink';
  }

  render() {
    return html`
      <ul class="swb-list">
        <slot/>
      </ul>
    `;
  }
}

customElements.define('swb-list', SwbListComponent);
