import {LitElement, html, css} from 'lit-element';
import resetCSS from '../../assets/styles/reset-css';

/**
 * @tag swb-list-item.
 * @Slot slot - List item content
 */
class SwbListItemComponent extends LitElement {
  static get styles() {
    return [
      resetCSS,
      css`
        :host {
          display: block;
        }
      `
    ]
  }

  constructor() {
    super();
  }

  render() {
    return html`
      <li class="swb-list__item">
        <slot></slot>
      </li>
    `;
  }
}

customElements.define('swb-list-item', SwbListItemComponent);
