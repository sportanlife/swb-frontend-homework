import {LitElement, html, css} from 'lit-element';
import resetCSS from '../../assets/styles/reset-css';

/**
 * @tag swb-label.
 * @Slot Label text.
 */
class SwbLabelComponent extends LitElement {
  static get styles() {
    return [
      resetCSS,
      css`
        :host {
          display: flex;
          width: 35%;
        }

        :host .swb-label {
          align-items: center;
          color: var(--color-almond);
          display: flex;
          height: var(--input-heigh);
          justify-content: flex-end;
          margin-right: var(--side-spacing-regular);
          width: 100%;
        }

        @media only screen and (max-width: 1024px) {
          :host {
            width: 100%;
          }

          :host .swb-label {
            justify-content: flex-start;
          }
        }
      `
    ];
  }

  render() {
    return html`
      <div class="swb-label">
        <slot></slot>
      </div>
    `;
  }
}

customElements.define('swb-label', SwbLabelComponent);
