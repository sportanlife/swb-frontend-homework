import {css, html, LitElement} from "lit-element";
import resetCSS from "../../assets/styles/reset-css";

/**
 * @tag swb-footer.
 * @Slot footer-group: swb-footer-group component
 * @Slot footer-info: footer info text
 */
class SwbFooterComponent extends LitElement {
  static get styles() {
    return [
      resetCSS,
      css`
        :host {
          flex-shrink: 0;
        }

        :host .swb-footer {
          background-color: var(--color-light-gray);
          min-height: 100px;
          padding: var(--side-spacing-regular) 0 0;
          width: 100%;
        }

        :host .swb-footer__top-bar {
          background: var(--gradient-orange-to-yellow);
          height: 8px;
          width: 100%;
        }

        :host .swb-footer__groups {
          display: flex;
          padding-bottom: var(--side-spacing-regular);
        }

        :host .swb-footer__info {
          background-color: var(--color-white);
          padding: var(--side-spacing-regular);
          text-align: center;
        }

        @media only screen and (max-width: 1024px) {
          :host .swb-footer__groups {
            flex-direction: column;
          }

          :host .swb-footer__info {
            padding: var(--side-spacing-small);
          }
        }
      `
    ];
  }

  render() {
    return html`
      <div class="swb-footer__top-bar"></div>
      <div class="swb-footer">
        <swb-container>
          <div class="swb-footer__groups">
            <slot name="footer-group"/>
          </div>
        </swb-container>
        <div class="swb-footer__info">
          <swb-container>
            <slot name="footer-info"/>
          </swb-container>
        </div>
      </div>
    `;
  }
}

customElements.define('swb-footer', SwbFooterComponent);
