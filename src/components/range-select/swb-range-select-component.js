import {LitElement, html, css} from 'lit-element';
import resetCSS from '../../assets/styles/reset-css';

/**
 * @tag swb-range-select.
 * @Prop max - Max value.
 * @Prop min - Min value.
 * @Prop step - Step.
 * @Prop value - Value
 */
class SwbRangeSelectComponent extends LitElement {
  static get styles() {
    return [
      resetCSS,
      css`
        :host {
          display: flex;
          max-width: 350px;
          flex: 1;
        }

        :host .swb-range-select__container {
          position: relative;
          width: 100%;
        }

        :host .swb-range-select {
          align-items: center;
          display: flex;
          position: relative;
        }

        :host .swb-range-select input[type=range] {
          -webkit-appearance: none;
          background: transparent; /* Otherwise white in Chrome */
          height: var(--input-heigh);
          position: relative;
          width: 100%; /* Specific width is required for Firefox. */
        }

        :host .swb-range-select input:focus {
          outline: none;
        }

        :host .swb-range-select input::-webkit-slider-thumb {
          -webkit-appearance: none;
          background: var(--color-orange-main);
          border-radius: 50%;
          cursor: pointer;
          height: 32px;
          margin-top: -10px; /* You need to specify a margin in Chrome, but in Firefox it is automatic */
          width: 32px;
          z-index: 1;
        }

        :host .swb-range-select input::-moz-range-thumb {
          background: var(--color-orange-main);
          border-color: transparent;
          border-radius: 50%;
          cursor: pointer;
          height: 32px;
          width: 32px;
        }

        :host .swb-range-select input[type=range]::-webkit-slider-runnable-track {
          width: 100%;
          background: var(--color-light-gray);
          border-radius: 5px;
          cursor: pointer;
          height: 10px;
        }

        :host .swb-range-select input[type=range]:focus::-webkit-slider-runnable-track {
          background: var(--color-light-gray);
          height: 10px;
        }

        :host .swb-range-select input[type=range]::-moz-range-track {
          background: var(--color-light-gray);
          border-radius: 5px;
          cursor: pointer;
          height: 10px;
          width: 100%;
        }

        :host .swb-range-select__progress-bar {
          background-color: var(--color-orange-main);
          border-radius: 5px;
          height: 10px;
          left: 3px;
          min-width: 30px;
          max-width: calc(100% - 16px);
          pointer-events: none;
          position: absolute;
          width: 0;
        }

        :host .swb-range-select__info {
          display: flex;
          width: 100%;
          justify-content: space-between;
          align-items: center;
          color: var(--color-almond);
        }

        :host .swb-range-select__tooltip-container {
          position: absolute;
        }

        :host .swb-range-select__tooltip {
          color: var(--color-orange-main);
          font-size: 18px;
          display: inline-block;
          position: relative;
          top: -70px;;
        }

        @media only screen and (max-width: 1024px) {
          :host {
            max-width: 100%;
          }

          :host .swb-range-select__info {
            font-size: var(--fs-small);
          }


          :host .swb-range-select__tooltip {
            padding-left: var(--side-spacing-regular);
            font-size: var(--fs-small);
          }
        }
      `
    ];
  }

  static get properties() {
    return {
      _currentValue: {type: Number},
      max: {type: Number},
      min: {type: Number},
      step: {type: Number},
      value: {type: Number},
    };
  }

  constructor() {
    super();

    this._currentValue = 0;
    this.max = 0
    this.min = 0
    this.step = 1;
    this.value = 0;
  }

  render() {
    return html`
      <div class="swb-range-select__container">
        <div class="swb-range-select">
          <input type="range" min="${this.min}" max="${this.max}" step="${this.step}" value="${this.value}">
          <div class="swb-range-select__progress-bar"></div>
        </div>
        <div class="swb-range-select__tooltip-container">
          <div class="swb-range-select__tooltip">
            ${this._currentValue} €
          </div>
        </div>
        <div class="swb-range-select__info">
          <div>${this.min} €</div>
          <div> ${this.max} €</div>
        </div>
      </div>
    `;
  }

  updated(_changedProperties) {
    super.updated(_changedProperties);
    this._calcTooltipPos();
    this._calcProgressBarWidth();
  }

  firstUpdated(_changedProperties) {
    super.firstUpdated(_changedProperties);
    this._initRangeSelect();
  }

  _initRangeSelect() {
    const select = this.shadowRoot.querySelector('.swb-range-select input');
    this._currentValue = select.value;
    select.addEventListener('input', () => {
      this._currentValue = select.value;
      const event = new CustomEvent('range-change', {
        bubbles: true,
        composed: true,
        detail: {value: this._currentValue}
      });
      this.dispatchEvent(event);
    });
  }

  _calcProgressBarWidth() {
    const progressBar = this.shadowRoot.querySelector('.swb-range-select__progress-bar');
    const select = this.shadowRoot.querySelector('.swb-range-select input');
    progressBar.style.width =
      Math.round(((select.value - this.min) / (this.max - this.min) - (this.step / this.max * 1.5)) * 100) + '%';
  }

  _calcTooltipPos() {
    const select = this.shadowRoot.querySelector('.swb-range-select input');
    const currentValueTooltip = this.shadowRoot.querySelector('.swb-range-select__tooltip');
    const left =
      (((select.value - this.min) / (this.max - this.min)) *
        ((select.clientWidth - 16) - 16)) + 16; // 16 is half size of thumb

    const tooltipRect = currentValueTooltip.getBoundingClientRect();
    const labelLeftPost = Math.round(left - (tooltipRect.right - tooltipRect.left) / 2);
    currentValueTooltip.style.left = labelLeftPost + 'px';
  }

  getCurrentValue() {
    return this._currentValue;
  }
}

customElements.define('swb-range-select', SwbRangeSelectComponent);
