import {LitElement, html, css} from 'lit-element';
import resetCSS from '../../assets/styles/reset-css';

/**
 * @tag swb-input.
 * @Prop error - For applying error styles.
 * @Prop type - Type.
 * @Prop value - Value.
 */
class SwbInputComponent extends LitElement {
  static get styles() {
    return [
      resetCSS,
      css`
        :host {
          display: flex;
          max-width: 350px;
          flex: 1
        }

        :host .swb-input {
          display: flex;
          flex-direction: column;
          width: 100%;
        }

        :host .swb-input__container {
          display: flex;
          flex: 1;
          flex-direction: column;
          justify-content: center;
          max-width: 350px;
          position: relative;
        }

        :host .swb-input input {
          background-color: var(--color-white);
          border: 1px solid var(--color-iceberg);
          border-radius: var(--border-radius-regular);
          height: var(--input-heigh);
          min-height: var(--input-heigh);
          padding: 0 var(--side-spacing-small);
          width: 100%;
        }

        :host([error]) .swb-input input,
        :host([error]) .swb-input input:focus {
          border-color: var(--color-red);
        }

        :host .swb-input__label {
          align-items: center;
          display: flex;
          height: var(--input-heigh);
          justify-content: flex-end;
          left: 0;
          padding-right: var(--side-spacing-regular);
          position: absolute;
          width: 35%;
        }

        :host([no-label='true']) .swb-input__label {
          display: none;
        }

        :host .swb-input__error-msg {
          color: var(--color-red);
          display: none;
        }

        :host([error]) .swb-input__error-msg {
          display: block;
        }

        :host .swb-input input:focus {
          border-color: var(--color-turquoise);
        }

        :host([type='select']) input {
          background-color: var(--color-apricot);
          pointer-events: none;
        }

        :host([type='select']) .swb-input__container:before,
        :host([type='select']) .swb-input__container:after {
          border-bottom: 7px solid var(--color-brown);
          border-left: 4px solid transparent;
          border-right: 4px solid transparent;
          content: '';
          right: var(--side-spacing-small);
          max-height: var(--input-heigh);
          position: absolute;
        }

        :host([type='select']) .swb-input__container:before {
          top: 14px;
        }

        :host([type='select']) .swb-input__container:after {
          bottom: 14px;
          transform: rotate(180deg);
        }

        @media (any-hover: hover) {
          :host([type='select']) input:hover {
            cursor: pointer;
          }
        }

        @media only screen and (max-width: 1024px) {
          :host {
            max-width: 100%;
          }

          :host .swb-input {
            flex-direction: column;
          }

          :host .swb-input__label {
            justify-content: flex-start;
            width: 100%;
          }

          :host .swb-input__container {
            max-width: 100%;
          }
        }
      `
    ];
  }

  static get properties() {
    return {
      _errorMessage: {type: String},
      error: {type: Boolean, reflect: true},
      type: {type: String},
      value: {type: String},
    };
  }

  constructor() {
    super();
    this._errorMessage = '';
    this.error = false;
    this.type = 'text';
    this.value = '';

    this.validate = () =>  this._initInputValidation();
  }

  connectedCallback() {
    super.connectedCallback();
  }

  render() {
    return html`
      <div class="swb-input">
        <div class="swb-input__container">
          <input value=${this.value} type="text">
        </div>
        <div class="swb-input__error-msg">${this._errorMessage}</div>
      </div>
    `;
  }

  firstUpdated(_changedProperties) {
    super.firstUpdated(_changedProperties);
    const input = this.shadowRoot.querySelector('input');
    if (this.type === 'select') {
      input.disabled = true;
    }
  }

  _initInputValidation() {
    const input = this.shadowRoot.querySelector('input');
    if(this.type === 'number') {
      const pattern = /^-?\d+\.?\d*$/;
      const regex = new RegExp(pattern);
      this.error = !regex.test(input.value);
      this._errorMessage = 'Amount accepts only numbers';
      return;
    }
    this.error = input.value.length === 0;
    this._errorMessage = 'This field cannot be empty';
  }
}

customElements.define('swb-input', SwbInputComponent);
