import {css, html, LitElement} from "lit-element";
import {iconStyles} from "../../assets/styles/icon-styles";
import resetCSS from "../../assets/styles/reset-css";
import socialIcons from "../../assets/dummy-data/social-icons";

/**
 * @tag swb-social-icons.
 * @Prop icons - List of icon names and urls.
 */
class SwbSocialIconsComponent extends LitElement {
  static get styles() {
    return [
      resetCSS,
      iconStyles,
      css`
        :host {
          display: block;
          margin-top: 20px;
        }

        :host .swb-social-icon {
          margin-right: 8px;
        }
      `
    ];
  }

  static get properties() {
    return {
      icons: {type: Array}
    };
  }

  constructor() {
    super();

    this.icons = socialIcons.icons;
  }

  render() {
    return html`
      ${this.icons.map(icon => (
          html`
            <swb-link href="${icon.url}" icon>
              <swb-icon class="swb-social-icon" name="${icon.name}" size="20"></swb-icon>
            </swb-link>
          `
      ))}
    `;
  }
}

customElements.define('swb-social-icons', SwbSocialIconsComponent);
