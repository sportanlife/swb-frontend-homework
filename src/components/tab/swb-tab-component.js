import {LitElement, html, css} from 'lit-element';
import resetCSS from '../../assets/styles/reset-css';

/**
 * @tag swb-tab.
 * @Slot Tab content.
 */
class SwbTabComponent extends LitElement {
  static get styles() {
    return [
      resetCSS,
      css`
        :host {
          display: flex;
        }

        :host .swb-tab {
          width: 100%;
        }

        :host .swb-tab__content {
          padding: var(--side-spacing-large) 0;
        }
      `
    ];
  }

  render() {
    return html`
      <div class="swb-tab">
        <swb-card>
          <div class="swb-tab__content">
            <slot></slot>
          </div>
        </swb-card>
      </div>
    `;
  }
}

customElements.define('swb-tab', SwbTabComponent);
