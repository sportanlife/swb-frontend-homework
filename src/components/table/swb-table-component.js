import {css, html, LitElement} from "lit-element";
import resetCSS from "../../assets/styles/reset-css";
import helperCSS from "../../assets/styles/helper";


/**
 * @tag swb-table.
 * @Prop header - Table header text.
 * @Prop data - Table data
 * @Slot header-link - Header link(s)
 */
class SwbTableComponent extends LitElement {
  static get styles() {
    return [
      resetCSS,
      helperCSS,
      css`
        :host {
          display: block;
          font-size: var(--fs-small);
        }

        :host .swb-table__header {
          align-items: center;
          display: flex;
          justify-content: space-between;
        }

        :host .swb-table__header-text {
          flex: 1;
        }

        :host .swb-table__data-table-container {
          display: flex;
          flex-direction: column;
          padding: var(--side-spacing-regular) 0;
        }

        :host .swb-table__header-row {
          align-items: center;
          background-color: var(--color-mountain-meadow);
          display: flex;
          font-size: var(--fs-small);
          min-height: 44px;
          padding: 0 var(--side-spacing-regular);
        }

        :host .swb-table__data-row {
          align-items: center;
          border-bottom: 1px solid var(--color-light-gray);
          display: flex;
          font-size: var(--fs-small);
          min-height: 40px;
          padding: 0 var(--side-spacing-regular);
        }

        :host .swb-table__data-cell {
          align-items: baseline;
          display: flex;
          flex-wrap: wrap;
        }

        :host .swb-table__data-cell--align-left {
          justify-content: flex-start;
        }

        :host .swb-table__data-cell--align-right {
          justify-content: flex-end;
        }

        :host .swb-table__footer-row {
          align-items: center;
          min-height: 50px;
          padding: 0 var(--side-spacing-regular);
        }

        :host ::slotted(swb-link:not(:last-of-type)) {
          margin-right: 8px;
        }

        @media only screen and (max-width: 1024px) {
          :host .swb-table__data-cell--hidden-sm {
            display: none;
          }

          :host .swb-table__header-row,
          :host .swb-table__data-row,
          :host .swb-table__footer-row {
            padding: 0 var(--side-spacing-small);
          }

          :host .swb-table__footer-last-cell {
            min-width: 50%;
          }
        }
      `
    ];
  }

  static get properties() {
    return {
      data: {type: Array},
      header: {type: String},
    };
  }

  constructor() {
    super();

    this.colQueries = [
      {size: 5, hiddenSm: false, alignDirection: 'left'},
      {size: 1, hiddenSm: true, alignDirection: 'right'},
      {size: 1, hiddenSm: true, alignDirection: 'right'},
      {size: 1, hiddenSm: true, alignDirection: 'right'},
      {size: 2, hiddenSm: false, alignDirection: 'right'}
    ];
    this.columnKeys = ['name', 'balance', 'credit', 'reserved', 'available'];
    this.columnTitles = ['Account', 'Balance', 'Credit', 'Reserved', 'Available'];
    this.data = [];

    this.total = {
      available: '456.56',
      balance: '5456.56',
      credit: '456.456',
      reserved: null
    }
  }

  render() {
    const dataTable = this.data.map((row, rowIndex) => {

      const tableRow = html`
        <div class="swb-table__data-row">${this.columnKeys.map((key, keyIndex) => {
          const cellData = row[key];
          const lastCellData = html`
            <div>${cellData}</div>
            <swb-text size="13">
              <swb-spacer size="5" direction="vertical"></swb-spacer>
              EUR
            </swb-text>
          `;
          
          const accountCellData = html`
            <swb-link class="swb-no-wrap" size="small" underline>${cellData}</swb-link>
            <swb-spacer direction="vertical" size="5"></swb-spacer>
            <swb-text size="13">
              ${row['accountNumber']}
            </swb-text>
          `
          
          return html`
            <div class="swb-table__data-cell  swb-table__data-cell--align-${this._setAlignDirection(keyIndex)} 
                 ${this._isHidden(keyIndex) ? 'swb-table__data-cell--hidden-sm' : ''}"
                 style="flex: ${this._setColSize(keyIndex)}">
              ${key === 'name' ? accountCellData : this._isLastCell(keyIndex) ? lastCellData : cellData}
            </div>
          `;
        })}
        </div>`

      if (rowIndex === 0) {
        return html`
          <div class="swb-table__header-row">
            ${this.columnTitles.map((title, titleIndex) => {
              return html`
                <div class=" swb-table__data-cell
                     swb-table__data-cell--align-${this._setAlignDirection(titleIndex)} 
                     ${this._isHidden(titleIndex) ? 'swb-table__data-cell--hidden-sm' : ''}"
                     style="flex: ${this._setColSize(titleIndex)};">${title}
                </div>
              `
            })}
          </div>
          ${tableRow}
        `
      }
      if (rowIndex === this.data.length - 1) {
        return html`
          ${tableRow}
          <div class="swb-table__footer-row" style="display: flex">
            ${this.columnTitles.map((title, footerIndex) => {
              const footerCellValue = this.total[this.columnKeys[footerIndex]];
              const footerFirstCell =
                  html`
                    <swb-text weight="bold" size="13">Total:</swb-text>`;

              const defaultFooterCellData =
                  html`
                    <swb-text weight="bold" size="13">${footerCellValue}</swb-text>
                  `;

              const lastFooterCellData =
                  html`
                    <swb-text size="24">${footerCellValue}</swb-text>
                    <swb-text size="13">
                      <swb-spacer size="5" direction="vertical"></swb-spacer>
                      EUR
                    </swb-text>
                  `;

              const footerCellData = footerIndex === 0 ? footerFirstCell :
                  this._isLastCell(footerIndex) ? lastFooterCellData : defaultFooterCellData;

              return html`
                <div class="swb-table__data-cell swb-table__data-cell--align-${this._setAlignDirection(footerIndex)}
                     ${this._isLastCell(footerIndex) ? 'swb-table__footer-last-cell' : ''} 
                     ${this._isHidden(footerIndex) ? 'swb-table__data-cell--hidden-sm' : ''}"
                     style="flex: ${this._setColSize(footerIndex)};">
                  ${footerCellData}
                </div>
              `
            })}
          </div>
        `
      }
      return tableRow;
    })

    return html`
      <div class="swb-table">
        <swb-card>
          <div class="swb-table__header">
            <h4 class="swb-table__header-text">${this.header}</h4>
            <div>
              <slot name="header-link"></slot>
            </div>
          </div>
          <div class="swb-table__data-table-container">
            ${dataTable}
          </div>
        </swb-card>
      </div>
    `;
  }

  _setAlignDirection(index) {
    return this.colQueries[index].alignDirection;
  }

  _setColSize(index) {
    return this.colQueries[index].size;
  }

  _isHidden(index) {
    return this.colQueries[index].hiddenSm;
  }

  _isLastCell(index) {
    return this.columnKeys.length - 1 === index;
  }
}

customElements.define('swb-table', SwbTableComponent);
