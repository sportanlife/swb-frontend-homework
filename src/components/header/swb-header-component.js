import {LitElement, html, css} from 'lit-element'
import resetCSS from '../../assets/styles/reset-css';
import menuItems from '../../assets/dummy-data/header-menu-items.js'

/**
 * @tag swb-header.
 * @Prop menuListItems - List of menu item names and icons.
 */
class SwbHeaderComponent extends LitElement {
  static get styles() {
    return [
      resetCSS,
      css`
        :host {
          display: flex;
          flex-shrink: 0;
        }

        :host .swb-header {
          background-color: var(--color-white);
          display: flex;
          flex-direction: column;
          position: relative;
          width: 100%;
          z-index: 2;
        }

        :host .swb-header__top-bar {
          background: var(--gradient-orange-to-yellow);
          height: 8px;
          width: 100%;
        }

        :host .swb-header-menu-bar {
          align-items: center;
          background-color: var(--color-white);
          display: flex;
          height: 60px;
          justify-content: space-between;
          padding: 0 var(--side-spacing-regular);
        }

        :host .swb-header-menu-bar .swb-logo {
          width: 148px;
        }

        :host .swb-burger-menu {
          align-items: center;
          cursor: pointer;
          display: none;
          justify-content: center;
          height: 60px;
          width: 60px;
        }

        :host .swb-burger-menu__items-container {
          justify-content: space-between;
          height: 16px;
          position: relative;
          width: 20px;
        }

        :host .swb-burger-menu__item {
          background-color: var(--color-brown);
          height: 3px;
          left: 0;
          opacity: 1;
          position: absolute;
          transform: rotate(0deg);
          transition: 0.1s ease-in-out;
          width: 100%;
        }

        :host .swb-burger-menu__item:first-of-type {
          top: 0
        }

        :host .swb-burger-menu__item:nth-of-type(2) {
          top: 8px
        }

        :host .swb-burger-menu__item:last-of-type {
          top: 16px;
        }

        :host([open]) .swb-burger-menu__item:nth-of-type(2) {
          opacity: 0;
          transform: rotate(45deg);
        }

        :host([open]) .swb-burger-menu__item:first-of-type {
          top: 7px;
          transform: rotate(45deg);
        }

        :host([open]) .swb-burger-menu__item:last-of-type {
          top: 7px;
          transform: rotate(-45deg);
        }

        :host .swb-header-menu-list {
          background-color: var(--color-white);
          display: flex;
        }

        :host .swb-header-menu-list__item {
          align-items: center;
          background-color: var(--color-white);
          border-top: 1px solid var(--color-light-gray);
          cursor: pointer;
          display: flex;
          flex: 1;
          flex-direction: column;
          font-size: var(--fs-large);
          justify-content: center;
          min-height: 54px;
          z-index: 1;
        }

        :host .swb-header-menu-list__item.active {
          color: var(--color-orange-main);
        }

        :host .swb-header-menu-list__item:not(:last-of-type) {
          border-right: 1px solid var(--color-light-gray);
        }

        :host .swb-menu-overlay {
          background-color: var(--color-light-wood);
          display: none;
          height: 100vh;
          opacity: 0.7;
          position: fixed;
          width: 100%;
        }

        :host([open]) .swb-menu-overlay {
          display: block;
        }

        @media (any-hover: hover) {
          :host .swb-header-menu-bar .swb-logo:hover {
            cursor: pointer;
          }

          :host .swb-header-menu-list__item:hover {
            background-color: var(--color-off-white);
            border-top-color: var(--color-orange-main);
          }

          :host .swb-header-menu-list__item:hover ~ .swb-menu-overlay {
            display: block;
          }
        }

        @media only screen and (max-width: 1024px) {
          :host .swb-header-menu-bar {
            padding-right: 0;
          }

          :host .swb-burger-menu {
            display: flex;
          }

          :host .swb-header-menu-list {
            display: none;
            flex-direction: column;
            position: fixed;
            top: 0;
            overflow-y: auto;
            width: 100%;

          }

          :host([open]) .swb-header-menu-list {
            display: flex;
            top: 68px;
          }

          :host .swb-header-menu-list__item {
            align-items: flex-start;
            flex: 0;
            padding: 0 var(--side-spacing-small);

          }

          :host .swb-header-menu-list__item.active {
            background-color: var(--color-off-white);
            border-top-color: var(--color-orange-main);
          }

          :host .swb-header-menu-list__item .menu-icon {
            display: none;
          }
        }
      `
    ];
  }

  static get properties() {
    return {
      menuListItems: {type: Array},
      open: {type: Boolean, reflect: true}
    };
  }

  constructor() {
    super();

    this.menuListitems = menuItems.data;
    this.open = false
  }

  render() {
    return html`
      <div class="swb-header">
        <div class="swb-header__top-bar"></div>
        <div class="swb-header-menu-bar">
          <a class="swb-logo" href="http://www.swedbank.ee">
            <img  src="assets/img/swedbank_logo.png" alt="swedbank logo">
          </a>
          <div @click=${() => this._toggleMenu()} class="swb-burger-menu">
            <div class="swb-burger-menu__items-container">
              <div class="swb-burger-menu__item"></div>
              <div class="swb-burger-menu__item"></div>
              <div class="swb-burger-menu__item"></div>
            </div>
          </div>
        </div>
        <ul id="header-menu-list" class="swb-header-menu-list">
          ${this.menuListitems.map((item, index) => (
                html`
                  <li @click=${(el) => this._onMenuItemSelect(el.target, index)}
                      class="swb-header-menu-list__item ${index === 0 ? 'active' : ''}">
                    <swb-icon class="menu-icon" name="${item.icon}"></swb-icon>
                    ${item.name}
                  </li>
                `
            ))}
          <div @click=${() => this._closeMenu()} class="swb-menu-overlay"></div>
        </ul>
      </div>
    `;
  }

  firstUpdated(_changedProperties) {
    super.firstUpdated(_changedProperties);
    this.open ? this._openMenu() : this._closeMenu();
  }

  _toggleMenu() {
    this.open = !this.open;
    document.body.classList.toggle('overflow-hidden');
  }

  _onMenuItemSelect(item, index) {
    const event = new CustomEvent('switch-content', {
      bubbles: true,
      composed: true,
      detail: {value: this.menuListitems[index].value}
    });

    this.shadowRoot.querySelectorAll('.swb-header-menu-list__item')
      .forEach(i => i.classList.remove('active'));
    item.classList.add('active');
    this.dispatchEvent(event);
  }

  _openMenu() {
    this.open = true;
    document.body.classList.add('overflow-hidden');
  }

  _closeMenu() {
    this.open = false;
    document.body.classList.remove('overflow-hidden');
  }
}

customElements.define('swb-header', SwbHeaderComponent);
