import {css, html, LitElement} from "lit-element";
import resetCSS from "../../assets/styles/reset-css";

/**
 * @tag swb-text.
 * @Prop color - text color.
 * @Prop size - size in pixels.
 * @Prop weight - font weight.
 * @Slot text.
 */
class SwbTextComponent extends LitElement {
  static get styles() {
    return [
      resetCSS,
      css`
        :host {
          display: inline-block;
        }

        :host .text-color-brown {
          color: var(--color-brown);
        }

        :host .text-color-disclaimer {
          color: var(--color-almond);
        }

        :host .text-color-orange {
          color: var(--color-orange-main);
        }

        :host .text-color-white {
          color: var(--color-white);
        }
      `
    ];
  }

  static get properties() {
    return {
      color: {type: String},
      size: {type: String},
      weight: {type: String}
    };
  }

  constructor() {
    super();

    this.color = 'brown';
    this.size = '15';
    this.weight = 'normal';
  }

  render() {
    return html`
      <div class="swb-text text-color-${this.color}" 
           style="font-size: ${this.size}px; font-weight: ${this.weight}">
        <slot/>
      </div>
    `;
  }
}

customElements.define('swb-text', SwbTextComponent);
