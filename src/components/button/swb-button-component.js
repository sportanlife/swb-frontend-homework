import {css, html, LitElement} from "lit-element";
import resetCSS from "../../assets/styles/reset-css";

/**
 * @tag swb-button.
 * @Prop type - button type.
 * @slot variant - Primary or secondary. Default primary.
 * @slot button text.
 */
class SwbButtonComponent extends LitElement {
  static get styles() {
    return [
      resetCSS,
      css`
        :host {
          display: inline-flex;
        }

        :host .swb-button {
          align-items: center;
          background-color: var(--color-orange-main);
          border: 1px solid var(--color-orange-main);
          border-radius: var(--border-radius-regular);
          color: var(--color-white);
          display: flex;
          flex-wrap: wrap;
          justify-content: center;
          height: 40px;
          padding: var(--btn-side-spacing);
          transition: var(--hover-bg-color-transition);
        }

        :host([variant='secondary']) .swb-button {
          background-color: var(--color-turquoise);
          border: 1px solid var(--color-turquoise);
        }

        @media (any-hover: hover) {
          :host .swb-button:hover {
            background-color: var(--color-orange-main-hover);
            cursor: pointer;
          }

          :host([variant='secondary']) .swb-button:hover {
            background-color: var(--color-turquoise-dark);
          }

          :host([disabled]) .swb-button:hover {
            cursor: default;
          }
        }

        @media only screen and (max-width: 768px) {
          :host {
            display: flex;
            width: 100%;
          }
          
          :host .swb-button {
            width: 100%;
          }
        }
      `
    ];
  }

  static get properties() {
    return {
      type: {type: String},
      variant: {type: String},
    };
  }

  constructor() {
    super();
    this.type = '';
    this.variant = 'primary'
  }

  render() {
    return html`
      <button type="${this.type}" class="swb-button">
        <slot/>
      </button>
    `;
  }
}

customElements.define('swb-button', SwbButtonComponent);