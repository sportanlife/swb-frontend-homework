import {LitElement, html, css} from 'lit-element'
import resetCSS from '../../assets/styles/reset-css';

/**
 * @tag swb-container.
 * @slot container content.
 */
class SwbContainerComponent extends LitElement {
  static get styles() {
    return [
      resetCSS,
      css`
        :host {
          display: flex;
        }

        :host .swb-container {
          margin: 0 auto;
          max-width: var(--container-max-width);
          padding: 0 var(--side-spacing-regular);
          width: 100%;
        }

        @media only screen and (max-width: 768px) {
          :host .swb-container {
            padding: 0 var(--side-spacing-small);
          }
        }
      `
    ];
  }

  render() {
    return html`
      <div class="swb-container">
        <slot/>
      </div>
    `;
  }
}

customElements.define('swb-container', SwbContainerComponent);
