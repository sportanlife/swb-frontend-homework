import {css, html, LitElement} from "lit-element";
import resetCSS from "../../assets/styles/reset-css";

/**
 * @tag swb-footer-group.
 * @Slot footer group item
 * @Prop title - Group title.
 */
class SwbFooterGroupComponent extends LitElement {
  static get styles() {
    return [
      resetCSS,
      css`
        :host {
          display: block;
          flex: 1;
        }

        :host .swb-footer-group__header {
          font-size: var(--fs-large);
          font-weight: var(--font-weight-regular);
          pointer-events: none;
        }

        :host .swb-footer-group__items {
          display: flex;
          flex-direction: column;
          margin-top: 10px;
        }

        :host ::slotted(swb-link) {
          margin-top: 7px;
        }

        @media only screen and (max-width: 1024px) {
          :host {
            border-bottom: 1px solid var(--color-wafer);
            margin-left: calc(-1 * var(--side-spacing-small));
            padding: 0 var(--side-spacing-small);
            width: calc(100% + var(--side-spacing-small) * 2);
          }

          :host .swb-footer-group__items {
            padding-bottom: 10px;
          }

          :host([mobile-compact]) .swb-footer-group__header {
            align-items: center;
            display: flex;
            height: 54px;
            position: relative;
            pointer-events: all;
          }

          :host([mobile-compact]) .swb-footer-group__header:after {
            content: '';
            position: absolute;
            right: var(--side-spacing-small);
            height: 10px;
            border-bottom: 2px solid var(--color-orange-main);
            border-right: 2px solid var(--color-orange-main);
            margin-top: -5px;
            width: 10px;
            transform: rotate(45deg);
          }

          :host([mobile-compact]) .swb-footer-group__items {
            margin-top: 0;
            max-height: 0;
            overflow: hidden;
            padding-bottom: 0;
            transition: max-height 0.25s cubic-bezier(0, 1, 0, 1);
          }

          :host([open]) .swb-footer-group__header:after {
            border-bottom-color: var(--color-brown);
            border-right-color: var(--color-brown);
            transform: rotate(226deg);
          }

          :host([open]) .swb-footer-group__header ~ .swb-footer-group__items {
            max-height: 1000px;
            transition: max-height 0.5s ease-in-out;
          }

          :host ::slotted(swb-link:last-of-type) {
            margin-bottom: 10px;
          }
        }
      `
    ];
  }

  static get properties() {
    return {
      header: {type: String},
      mobileCompact: {type: Boolean, reflect: true},
      open: {type: Boolean, reflect: true}
    };
  }

  constructor() {
    super();
    this.mobileCompact = false;
    this.open = false;
  }

  render() {
    return html`
      <div class="swb-footer-group">
        <h3 class="swb-footer-group__header" @click=${el => this._onHeaderClick(el)}>${this.header}</h3>
        <div class="swb-footer-group__items">
          <slot></slot>
        </div>
      </div>
    `
  }

  _onHeaderClick(el) {
    this.open = !this.open;
  }
}

customElements.define('swb-footer-group', SwbFooterGroupComponent);
