import {css, html, LitElement} from "lit-element";
import resetCSS from "../../assets/styles/reset-css";

/**
 * @tag swb-link.
 * @Prop href - url.
 * @Prop underline - Decorates link with underline and removes prefix triangle
 * @Prop light - Light color
 * @Prop size - Size in pixels
 * @Prop icon - Removes default link styles, prefixes etc.
 * @Slot link text
 */
class SwbLinkComponent extends LitElement {
  static get styles() {
    return [
      resetCSS,
      css`
        :host {
          display: inline-flex;
        }

        :host .swb-link a {
          align-items: center;
          color: var(--color-brown);
          display: flex;
        }

        :host .swb-link a:before {
          border-bottom: 4px solid transparent;
          border-left: 4px solid transparent;
          border-right: 4px solid;
          border-top: 4px solid;
          content: '';
          margin-right: 8px;
          transform: rotate(45deg);
        }

        :host([underline]) .swb-link a {
          color: var(--color-turquoise-dark);
          text-decoration: underline;
        }

        :host([light]) .swb-link a {
          color: var(--color-turquoise-dark);
        }

        :host([underline]) .swb-link a:before,
        :host([icon]) .swb-link a:before{
          content: none;
        }

        :host([size="small"]) .swb-link a {
          font-size: var(--fs-small);
        }

        @media (any-hover: hover) {
          :host(:not([icon])) .swb-link a:hover {
            cursor: pointer;
            text-decoration: underline;
          }
        }
      `
    ];
  }

  static get properties() {
    return {
      href: {type: String, reflect: true},
      icon: {type: Boolean, reflect: true},
      light: {type: Boolean, reflect: true},
      size: {type: String},
      underline: {type: Boolean, reflect: true}
    };
  }

  constructor() {
    super();

    this.size = '';
    this.underline = false;
  }

  render() {
    return html`
      <div class="swb-link">
        ${this.href ?
            html`
              <a href=${this.href}>
                <slot/>
              </a>` :
            html`
              <a>
                <slot></slot>
              </a>`}
      </div>
    `;
  }
}

customElements.define('swb-link', SwbLinkComponent);
