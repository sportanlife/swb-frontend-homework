import {LitElement, html, css} from 'lit-element';
import resetCSS from '../../assets/styles/reset-css';

/**
 * @tag swb-tabs.
 * @Prop active-tab - Default active tab. !NB count starts from 0
 * @Prop tabs - Tabs names.
 */
class SwbTabsComponent extends LitElement {
  static get styles() {
    return [
      resetCSS,
      css`
        :host {
          display: flex;
        }

        :host .swb-tabs {
          width: 100%;
        }

        :host .swb-tabs__header {
          display: flex;
        }

        :host .swb-tabs__header-btn {
          align-items: center;
          background-color: var(--color-off-white);
          border-top-right-radius: 3px;
          cursor: pointer;
          display: flex;
          justify-content: center;
          min-height: 50px;
          padding: 0 var(--side-spacing-regular);
          transition: var(--hover-bg-color-transition);
        }

        :host .swb-tabs__header-btn[active] {
          border-top-left-radius: 3px;
        }

        :host .swb-tabs__header-btn[active] {
          background-color: white;
        }

        :host ::slotted([slot='tab']) {
          position: absolute;
          transform: translateX(-100vw);
        }

        :host ::slotted([slot='tab'][active]) {
          position: relative;
          transform: translateX(0);
          z-index: 1;
        }

        @media (any-hover: hover) {
          :host .swb-tabs__header-btn:not([active]):hover {
            background-color: var(--color-bisque);
          }
        }
      `
    ];
  }

  static get properties() {
    return {
      tabs: {type: Array},
      activeTab: {type: Number}
    };
  }

  constructor() {
    super();

    this._currentActive = 0;
    this.activeTab = 0;
    this.tabs = ['Payment', 'Calculator'];
  }

  render() {
    return html`
      <div class="swb-tabs">
        <div class="swb-tabs__header">
          ${this.tabs.map((tab, index) => {
            return html`
              <div @click=${() => this._onTabBtnClick(index)} class="swb-tabs__header-btn">
                ${tab}
              </div>
            `
          })}
        </div>
        <div class="swb-tabs__content">
          <slot name="tab"></slot>
        </div>
      </div>
    `;
  }

  firstUpdated(_changedProperties) {
    super.firstUpdated(_changedProperties);
    this._currentActive = this.activeTab;
    this._setActiveTab();
  }

  _onTabBtnClick(index) {
    this._currentActive = index;
    this._setActiveTab();
  }

  _setActiveTab() {
    const tabs = this.querySelectorAll('swb-tab');
    const tabButtons = this.shadowRoot.querySelectorAll('.swb-tabs__header-btn');
    tabButtons.forEach(btn => btn.removeAttribute('active'));
    tabs.forEach(tab => tab.removeAttribute('active'));
    tabButtons[this._currentActive].setAttribute('active', 'true');
    tabs[this._currentActive].setAttribute('active', 'true');
  }
}

customElements.define('swb-tabs', SwbTabsComponent);
