import {LitElement, html, css} from 'lit-element'
import resetCSS from '../../assets/styles/reset-css';

/**
 * @tag swb-card.
 * @slot card content.
 */
class SwbCardComponent extends LitElement {
  static get styles() {
    return [
      resetCSS,
      css`
        :host {
          display: flex;
        }

        :host .swb-card {
          background-color: var(--color-white);
          padding: var(--side-spacing-regular);
          width: 100%;
        }

        @media only screen and (max-width: 768px) {
          :host .swb-card {
            padding: var(--side-spacing-small);
          }
        }
      `
    ];
  }

  render() {
    return html`
      <div class="swb-card">
        <slot/>
      </div>
    `;
  }
}

customElements.define('swb-card', SwbCardComponent);
