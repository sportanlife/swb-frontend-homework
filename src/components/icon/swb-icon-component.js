import {LitElement, html, css} from 'lit-element';
import resetCSS from '../../assets/styles/reset-css';
import {iconStyles} from "../../assets/styles/icon-styles";

/**
 * @tag swb-icon.
 * @Prop name - Name.
 * @Prop size - Size in pixels.
 * @Prop suffix - Suffix text.
 */
class SwbIconComponent extends LitElement {
  static get styles() {
    return [
      resetCSS,
      iconStyles,
      css`
        :host {
          display: inline-block;
        }

        :host .swb-icon {
          align-items: center;
          display: flex;
        }

        :host .swb-icon__suffix {
          color: var(--color-turquoise-dark);
          font-size: var(--fs-small);
          margin-left: 5px;
        }
      `
    ]
  }

  static get properties() {
    return {
      name: {type: String},
      size: {type: String},
      suffix: {type: String}
    };
  }

  constructor() {
    super();

    this.name = '';
    this.size = '18';
  }

  render() {
    return html`
      <div class="swb-icon">
        <i class="swb-icon icon-${this.name}" style="font-size: ${this.size}px"></i>
        ${this.suffix &&
        html`
          <div class="swb-icon__suffix">${this.suffix}</div>
        `}
      </div>
    `;
  }
}

customElements.define('swb-icon', SwbIconComponent);
