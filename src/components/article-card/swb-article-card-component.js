import {css, html, LitElement} from "lit-element";
import resetCSS from "../../assets/styles/reset-css";

/**
 * @tag swb-article-card.
 * @Slot card content.
 * @Prop background-color - Header background color. Default - pink
 * @Prop header - Header text
 */

export class SwbArticleCardComponent extends LitElement {
  static get styles() {
    return [
      resetCSS,
      css`
        :host {
          display: flex;
          width: 100%;
        }

        :host .swb-article-card {
          display: flex;
          flex-direction: column;
          width: 100%;
        }

        :host .swb-article-card__header {
          align-items: center;
          background-color: var(--color-pink);
          color: var(--color-white);
          display: flex;
          font-size: var(--fs-large);
          font-weight: var(--font-weight-bold);
          height: 40px;
          padding: 10px;
          position: relative;
          width: 100%;
        }

        :host .swb-article-card__body {
          background-color: var(--color-apricot);
          padding: 25px var(--side-spacing-small) var(--side-spacing-regular);
          flex: 1;
        }

        :host .swb-article-card__header:after {
          border-bottom: 10px solid var(--color-pink);
          border-left: 10px solid transparent;
          border-right: 10px solid var(--color-pink);
          border-top: 10px solid transparent;
          bottom: -9px;
          content: '';
          left: 20px;
          position: absolute;
          transform: rotate(45deg);
        }

        :host([header-bg-color='yellow']) .swb-article-card__header {
          background-color: var(--color-yellow);
        }

        :host([header-bg-color='blue']) .swb-article-card__header {
          background-color: var(--color-blue);
        }

        :host([header-bg-color='blue']) .swb-article-card__header:after {
          border-bottom-color: var(--color-blue);
          border-right-color: var(--color-blue);
        }

        :host([header-bg-color='yellow']) .swb-article-card__header:after {
          border-bottom-color: var(--color-yellow);
          border-right-color: var(--color-yellow);
        }

        @media only screen and (max-width: 768px) {
          :host .swb-footer__groups {
            flex-direction: column;
          }

          :host .swb-footer__info {
            padding: var(--side-spacing-small);
          }
        }
      `
    ];
  }

  static get properties() {
    return {
      header: {type: String},
      headerBgColor: {type: String, reflect: true},
    };
  }

  constructor() {
    super();

    this.header = '';
    this.headerBgColor = 'pink';
  }

  render() {
    return html`
      <div class="swb-article-card">
        <div class="swb-article-card__header">
          ${this.header}
        </div>
        <div class="swb-article-card__body">
          <slot/>
        </div>
      </div>
    `;
  }
}

customElements.define('swb-article-card', SwbArticleCardComponent);