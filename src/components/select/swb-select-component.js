import {LitElement, html, css} from 'lit-element';
import resetCSS from '../../assets/styles/reset-css';

let selectId = 0;

/**
 * @tag swb-select.
 * @Prop id - Id.
 * @Prop open - Open state.
 * @Prop options - Options for dropdown.
 */
class SwbSelectComponent extends LitElement {
  static get styles() {
    return [
      resetCSS,
      css`
        :host {
          display: flex;
          flex: 1;
          max-width: 350px;
        }

        :host .swb-select {
          position: relative;
          width: 100%;
        }

        :host .swb-select__options {
          background-color: var(--color-apricot);
          border: 1px solid;
          display: none;
          max-width: 350px;
          position: absolute;
          top: var(--input-heigh);
          width: 100%;
          z-index: 1;
        }

        :host .swb-select__options-item {
          padding: 0 var(--side-spacing-small);
        }

        :host .swb-select__options-item.selected {
          color: var(--color-white);
          background-color: var(--color-light-blue);
        }

        :host([open]) .swb-select__options {
          display: block;
        }

        :host([fix-width]) {
          flex: 0;
          min-width: 100px;
          width: 100px;
        }

        @media (any-hover: hover) {
          :host .swb-select:hover {
            cursor: pointer;
          }

          :host .swb-select__options-item:hover {
            background-color: var(--color-light-blue);
            color: var(--color-white);
            cursor: pointer;
          }
        }

        @media only screen and (max-width: 1024px) {
          :host,
          :host([fix-width]) {
            max-width: 100%;
            width: 100%;
          }

          :host .swb-select__options {
            max-width: 100%;
          }
        }
      `
    ];
  }

  static get properties() {
    return {
      _currentValue: {type: Object},
      id: {type: String},
      open: {type: Boolean, reflect: true},
      options: {type: Array}
    };
  }

  constructor() {
    super();
    this.id = 'selectId-' + selectId++;
    this.options = [];
    this.open = false;
    this.validate = () => this._validateSelect();
  }

  render() {
    return html`
      <div class="swb-select">
        <swb-input
            @click="${(e) => this._onSelectClick(e)}"
            value=${this._currentValue?.name}
            type="select">
        </swb-input>
        <div class="swb-select__options">
          ${this.options.map((option, index) => {
            return html`
              <div
                  class="swb-select__options-item 
                  ${this._currentValue === option ? 'selected' : ''}"
                  @click=${() => this._selectOption(index)}>${option.name}
              </div>
            `
          })}
        </div>
      </div>
    `;
  }

  connectedCallback() {
    super.connectedCallback();
    document.addEventListener('touch-select', (e) =>  this._toggle(e));
    document.addEventListener('click', () => this._close());
  }

  firstUpdated(_changedProperties) {
    super.firstUpdated(_changedProperties);
    super.firstUpdated(_changedProperties);
    this._initSelect();
  }

  disconnectedCallback() {
    super.disconnectedCallback();
    document.removeEventListener('toggle-select', this._toggle);
    document.removeEventListener('click', this._close);
  }

  _initSelect() {
    if (this.options.length === 0) {
      return;
    }

    this._currentValue = this._currentValue ? this._currentValue : this.options[0];
  }

  _onSelectClick(e) {
    e.stopPropagation();
    this.open = !this.open;
    const event = new CustomEvent('touch-select', {
      bubbles: true,
      composed: true,
      detail: {id: this.id}
    });
    this.dispatchEvent(event);
  }

  _selectOption(index) {
    if (this._currentValue !== this.options[index]) {
      this._currentValue = this.options[index];
    }
    this.open = false;
    const event = new CustomEvent('select-option-change', {
      bubbles: true,
      composed: true,
      detail: {value: this._currentValue}
    });
    this.dispatchEvent(event);
  }

  _close() {
    this.open = false;
  }

  _toggle(event) {
    this.open = this.open && this.id === event.detail.id;
  }

  _validateSelect() {
    this.shadowRoot.querySelector('swb-input').validate();
  }

  getCurrentValue() {
    return this._currentValue;
  }
}

customElements.define('swb-select', SwbSelectComponent);
