import {LitElement, html, css} from 'lit-element';
import resetCSS from '../../assets/styles/reset-css';

/**
 * @tag swb-banner.
 * @Prop header - Header text.
 * @Prop no-cta - If cta group no needed.
 * @Slot banner-figure-content - Left side figure content(text, icon and etc.).
 * @Slot content - Content.
 * @Slot cta-link - swb-link.
 * @Slot cta-btn - swb-button.
 */
class SwbBannerComponent extends LitElement {
  static get styles() {
    return [
      resetCSS,
      css`
        :host {
          display: flex;
          width: 100%;
        }

        :host .swb-banner__container {
          width: 100%;
        }

        :host .swb-banner__figure {
          align-items: center;
          background-color: var(--color-yellow);
          border-radius: 50%;
          display: flex;
          flex-shrink: 0;
          height: 150px;
          justify-content: center;
          margin-right: var(--side-spacing-regular);
          position: relative;
          width: 150px;
        }

        :host .swb-banner__figure:before {
          border-left: 34px solid transparent;
          border-right: 34px solid transparent;
          border-bottom: 54px solid var(--color-yellow);
          bottom: -24px;
          content: '';
          position: absolute;
          transform: rotate(180deg);
        }

        :host .swb-banner {
          background-color: var(--color-apricot);
          display: flex;
          padding: var(--side-spacing-regular) var(--side-spacing-regular) var(--banner-bottom-spacing-large);
        }

        :host([no-cta]) .swb-banner {
          padding-bottom: 46px;
        }

        :host .swb-banner__header {
          font-size: var(--fs-large);
          font-weight: var(--font-weight-bold);
          margin-bottom: var(--side-spacing-regular);
        }

        :host .swb-banner__content-container {
          display: flex;
          justify-content: center;
          flex-direction: column;
          position: relative;
        }

        :host .swb-banner__content {
          font-size: var(--fs-small);
        }

        :host .swb-banner__cta-group {
          align-items: center;
          display: flex;
          justify-content: space-between;
          position: absolute;
          bottom: -46px;
          width: 100%;
        }

        :host([no-cta]) .swb-banner__cta-group {
          display: none;
        }

        @media only screen and (max-width: 768px) {
          :host .swb-banner {
            flex-direction: column;
            padding: var(--side-spacing-small);
          }

          :host .swb-banner__figure {
            margin: 0 auto;
          }

          :host .swb-banner__content-container {
            margin-top: 40px;
          }

          :host .swb-banner__cta-group {
            align-items: flex-start;
            bottom: 0;
            flex-direction: column;
            position: relative;
          }

          :host .swb-banner__cta-group-item {
            margin-top: var(--side-spacing-small);
            width: 100%;
          }
        }
      `
    ];
  }

  static get properties() {
    return {
      header: {type: String},
      noCta: {type: Boolean, reflect: true}
    };
  }

  constructor() {
    super();

    this.header = '';
  }

  render() {
    return html`
      <div class="swb-banner__container">
        <swb-card>
          <div class="swb-banner">
            <div class="swb-banner__figure">
              <div class="swb-banner__figure-text">
                <slot name="banner-figure-content" />
              </div>
            </div>
            <div class="swb-banner__content-container">
              <div class="swb-banner__header">${this.header}</div>
              <div class="swb-banner__content">
                <slot name="content"/>
              </div>
              <div class="swb-banner__cta-group">
                <div class="swb-banner__cta-group-item">
                  <slot name="cta-link"/>
                </div>
                <div class="swb-banner__cta-group-item">
                  <slot name="cta-btn"/>
                </div>
              </div>
            </div>
          </div>
        </swb-card>
      </div>
    `;
  }
}

customElements.define('swb-banner', SwbBannerComponent);
