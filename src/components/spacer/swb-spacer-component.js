import {css, html, LitElement} from "lit-element";
import resetCSS from "../../assets/styles/reset-css";

/**
 * @tag swb-spacer.
 * @Prop size - Size in pixels.
 * @Prop direction - Direction vertical or horizontal. Default horizontal.
 */
class SwbSpacerComponent extends LitElement {
  static get styles() {
    return [
      resetCSS,
      css`
        :host {
          display: block;
          min-height: 0;
        }
        
        :host([direction='vertical']) {
          display: inline-block;
        }
      `
    ];
  }

  static get properties() {
    return {
      size: {type: String},
      direction: {type: String, reflect: true}
    };
  }

  constructor() {
    super();

    this.size = '0';
    this.direction = 'horizontal';
  }

  render() {
    return html`
      <div style="${this.direction !== 'vertical' ? 'height:' : 'width:'}${this.size}px" ></div>
    `;
  }
}

customElements.define('swb-spacer', SwbSpacerComponent);
