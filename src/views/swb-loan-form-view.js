import {LitElement, html, css} from 'lit-element';
import resetCSS from '../assets/styles/reset-css';
import formCSS from '../assets/styles/form';
import helperCSS from '../assets/styles/helper';

/**
 * @tag swb-loan-form-view.
 */
class SwbLoanFormView extends LitElement {
  static get styles() {
    return [
      resetCSS,
      formCSS,
      helperCSS,
      css`
        :host {
          display: flex;
          width: 100%;
        }

        :host form {
          width: 100%;
          display: flex;
          flex-direction: column;
        }

        :host .form-grid {
          display: grid;
          grid-auto-flow: column;
          grid-template-columns: 8fr 4fr;
          grid-column-gap: var(--side-spacing-regular);
        }

        :host .form-grid > :nth-child {
          padding: 0 var(--side-spacing-regular);
        }

        :host .form-grid > :not(:last-child) {
          border-right: 1px solid var(--color-light-gray);
        }

        :host .form-cta-group {
          border-top: 1px solid var(--color-light-gray);
          margin: var(--side-spacing-regular) 0;
          padding-top: var(--side-spacing-regular);
          display: flex;
          justify-content: flex-end;
        }

        :host .form-mobile-title {
          display: none;
        }

        @media only screen and (max-width: 1024px) {
          :host .form-grid {
            grid-auto-flow: row;
            grid-gap: var(--side-spacing-small);
            grid-template: none;
          }

          :host .form-grid > :not(:last-child) {
            border: none;
          }

          :host .form-grid > :last-child {
            margin-top: var(--side-spacing-small);
          }

          :host .form-mobile-title {
            display: block;
          }
        }
      `
    ];
  }

  static get properties() {
    return {
      data: {type: Object},
      _monthlyPayment: {type: Number}
    };
  }

  constructor() {
    super();

    this.data = [];
    this._monthlyPayment = 0;
  }

  render() {
    return html`
      <form>
        <div class="form-grid">
          <div>
            <div class="hidden-lg">
              <swb-text color="orange" size="18" weight="bold">Choose the loan size</swb-text>
              <swb-spacer size="40"></swb-spacer>
            </div>
            <div class="swb-form-row">
              <swb-label class="hidden-md">Loan size</swb-label>
              <swb-range-select
                  id="loan-size"
                  .min="${this.data.loan.min}"
                  .max="${this.data.loan.max}"
                  .step="${2000}"
                  .value="${this.data.loan.defaultValue}">
              </swb-range-select>
            </div>
            <div class="swb-form-row">
              <swb-label>Period</swb-label>
              <swb-select class="mb-lg-10"
                          id="loan-period"
                          .options="${this.data.period}"
                          fix-width></swb-select>
            </div>
            <div class="swb-form-row">
              <swb-label>Interest</swb-label>
              <swb-select class="mb-lg-10"
                          id="loan-interest"
                          .options="${this.data.interest}"
                          fix-width></swb-select>
            </div>
          </div>
          <div>
            <div class="swb-flex swb-justify-between">
              <swb-text>Monthly payment</swb-text>
              <div>
                <swb-text color="orange" size="24">
                  ${this._monthlyPayment}
                  <swb-text size="13">EUR</swb-text>
                </swb-text>
              </div>
            </div>
            <div class="form-cta-group">
              <swb-button>Apply</swb-button>
            </div>
          </div>
        </div>
      </form>
    `;
  }

  connectedCallback() {
    super.connectedCallback();
    this.addEventListener('range-change', async (e) => {
      await this.requestUpdate().then(() => this.calcLoanPayment());
    });
    this.addEventListener('select-option-change', async (e) => {
      await this.requestUpdate().then(() => this.calcLoanPayment());
    });
  }

  firstUpdated(_changedProperties) {
    super.firstUpdated(_changedProperties);
    this.updateComplete.then(() => this.calcLoanPayment());
  }

  disconnectedCallback() {
    super.disconnectedCallback();
    this.removeEventListener('select-option-change', this.calcLoanPayment);
    this.removeEventListener('range-change', this.calcLoanPayment);
  }

  calcLoanPayment() {
    const loanSizeSelect = this.shadowRoot.querySelector('#loan-size');
    const loanPeriodSelect = this.shadowRoot.querySelector('#loan-period');
    const loanInterestSelect = this.shadowRoot.querySelector('#loan-interest');

    //Principal value
    const pv = loanSizeSelect.getCurrentValue();

    //Interest per month
    const interest = loanInterestSelect.getCurrentValue().value / 100 / 12;

    //Total payments
    const payments = loanPeriodSelect.getCurrentValue().value * 12;

    //Future value
    const fv = Math.pow(1 + interest, payments);
    const monthlyPayment = (pv * fv * interest) / (fv - 1);
    if (
      !isNaN(monthlyPayment) &&
      (monthlyPayment !== Number.NEGATIVE_INFINITY) &&
      (monthlyPayment !== Number.POSITIVE_INFINITY)
    ) {
      return this._monthlyPayment = this.round(monthlyPayment);
    }
  }

  round(number) {
    return Math.round(number * 10) / 10;
  }

}

customElements.define('swb-loan-form-view', SwbLoanFormView);
