import {LitElement, html, css} from 'lit-element';
import resetCSS from '../assets/styles/reset-css';
import helperCSS from '../assets/styles/helper';
import tableData from '../assets/dummy-data/table-data';

/**
 * @tag swb-home-page-view.
 */
class SwbHomePageView extends LitElement {
  static get styles() {
    return [
      resetCSS,
      helperCSS,
      css`
        :host {
          display: block;
          width: 100%;
        }
      `
    ];
  }

  static get properties() {
    return {
      data: {type: Object}
    };
  }

  constructor() {
    super();
  }

  render() {
    return html`
      <swb-container>
        <swb-text size="36" color="orange" weight="bold">Home</swb-text>
        <swb-spacer size="20"></swb-spacer>
      </swb-container>
      <swb-table .data="${tableData.rows}" header="Your Swedbank overview">
        <swb-link slot="header-link" icon>
          <swb-icon name="pdf" size="24" suffix="PDF"></swb-icon>
        </swb-link>
        <swb-link slot="header-link" icon>
          <swb-icon name="pdf" size="24" suffix="XSL"></swb-icon>
        </swb-link>
      </swb-table>
      <swb-spacer size="10"></swb-spacer>
      <swb-card>
        <div class="swb-card-grid">
          <swb-article-card header="Open"
                            header-bg-color="blue">
            <h4>One of the core values of Swedbnak</h4>
            <swb-spacer size="5"></swb-spacer>
            <p>Lorem ipsum dolor sit amet, consectetur adipiscing elit</p>
          </swb-article-card>
          <swb-article-card header="Caring"
                            header-bg-color="yellow">
            Ut posuere turpis ac auctor aliquet. Aenean semper, leo mollis hendrerit dapibus,
            odio nisi imperdiet ipsum
            <swb-spacer size="10"></swb-spacer>
            <swb-link light>Read more</swb-link>
          </swb-article-card>
          <swb-article-card header="Simple">
            <swb-list>
              <swb-list-item>Lorem ipsum dolor sit amet, consectetur adipiscing elit</swb-list-item>
              <swb-list-item>Lorem ipsum dolor sit amet, consectetur adipiscing elit</swb-list-item>
            </swb-list>
          </swb-article-card>
        </div>
      </swb-card>
    `;
  }

}

customElements.define('swb-home-page-view', SwbHomePageView);
