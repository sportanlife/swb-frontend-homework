import {LitElement, html, css} from 'lit-element';
import resetCSS from '../assets/styles/reset-css';
import helperCSS from '../assets/styles/helper';
import loanFormData from '../assets/dummy-data/loan-form-data';
import paymentFormData from '../assets/dummy-data/payment-form-data';

/**
 * @tag swb-banking-page-view.
 */
class SwbBankingPageView extends LitElement {
  static get styles() {
    return [
      resetCSS,
      helperCSS,
      css`
        :host {
          display: block;
          width: 100%;
        }
      `
    ];
  }

  static get properties() {
    return {
      data: {type: Object}
    };
  }

  constructor() {
    super();
  }

  render() {
    return html`
      <swb-container>
        <swb-text size="36" color="orange" weight="bold">Everyday banking</swb-text>
        <swb-spacer size="20"></swb-spacer>
      </swb-container>
      <swb-tabs .activeTab="${0}">
        <swb-tab slot="tab">
          <swb-payment-form-view .data="${paymentFormData}"></swb-payment-form-view>
        </swb-tab>
        <swb-tab slot="tab">
          <swb-loan-form-view .data="${loanFormData}"></swb-loan-form-view>
        </swb-tab>
      </swb-tabs>
      <swb-spacer size="10"></swb-spacer>
      <swb-banner header="Welcome to Swedbank!">
        <swb-text
            slot="banner-figure-content"
            color="white" size="18"
            weight="bold">
          Hello world!
        </swb-text>
        <div slot="content">
          With 7.2 million private customers and more than 574 000 corporate and organisation customers.
          This makes us Sweden's largest bank in terms of number of customers and gives us a leading
          position in our other home markets of Estonia, Latvia and Lithuania. As a major bank, we are a
          significant part of the financial system and play an important role in the local communities we
          serve. We are dedicated to helping our customers, our shareholders and society as whole stay
          financially sound and sustainable.
        </div>
        <swb-link slot="cta-link" size="small" light>Read more</swb-link>
        <swb-button slot="cta-btn">Go</swb-button>
      </swb-banner>
    `;
  }
}

customElements.define('swb-banking-page-view', SwbBankingPageView);
