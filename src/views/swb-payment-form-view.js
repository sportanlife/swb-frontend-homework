import {LitElement, html, css} from 'lit-element';
import resetCSS from '../assets/styles/reset-css';
import formCSS from '../assets/styles/form';
import helperCSS from '../assets/styles/helper';

/**
 * @tag swb-payment-form-view.
 */
class SwbPaymentFormView extends LitElement {
  static get styles() {
    return [
      resetCSS,
      formCSS,
      helperCSS,
      css`
        :host {
          display: flex;
          width: 100%;
        }

        :host form {
          width: 100%;
          display: flex;
          flex-direction: column;
        }
      `
    ];
  }

  static get properties() {
    return {
      data: {type: Object}
    };
  }

  constructor() {
    super();

    this.data = [];
  }

  render() {
    return html`
      <form>
        <div class="swb-form-row">
          <swb-label>Account</swb-label>
          <swb-select class="mb-lg-10 js-form-element" 
                      .options="${this.data.account}" 
                      required></swb-select>
        </div>
        <div class="swb-form-row">
          <swb-label>Saved payments</swb-label>
          <swb-select class="mb-lg-10 js-form-element" 
                      .options="${this.data.savedPayments}"
                      required></swb-select>
        </div>
        <div class="swb-form-row">
          <swb-label>Amount</swb-label>
          <div class="swb-input-group">
            <swb-input class="mb-lg-10 js-form-element" 
                       type="number" required>
            </swb-input>
            <swb-select class="mb-lg-10 js-form-element" 
                        .options="${this.data.currency}" 
                        fix-width
                        required>
            </swb-select>
          </div>
        </div>
        <div class="swb-form-row">
          <swb-label>Description</swb-label>
          <swb-input class="mb-lg-10 js-form-element" type="text" required></swb-input>
        </div>
        <div class="swb-cta-group">
          <swb-button variant="secondary">Save</swb-button>
          <swb-button @click=${() => this.validateForm()}>Pay</swb-button>
        </div>
      </form>
      
    `;
  }

  validateForm() {
    this.shadowRoot.querySelectorAll('.js-form-element').forEach(formItem => {
      if (formItem.hasAttribute('required')) {
        formItem.validate();
      }
    });
  }
}

customElements.define('swb-payment-form-view', SwbPaymentFormView);
