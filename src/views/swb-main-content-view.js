import {LitElement, html, css} from 'lit-element';
import resetCSS from '../assets/styles/reset-css';
import helperCSS from '../assets/styles/helper';

const homeView = html`
  <swb-home-page-view></swb-home-page-view>`
const bankingView = html`
  <swb-banking-page-view></swb-banking-page-view>`
const notFound = html`<h2>Page not found</h2>`

/**
 * @tag swb-main-content-view.
 * @Prop currentView - Current view.
 */
class SwbMainContentView extends LitElement {
  static get styles() {
    return [
      resetCSS,
      helperCSS,
      css`
        :host {
          display: block;
          margin: var(--side-spacing-regular) 0;
          width: 100%;
        }
      `
    ];
  }

  static get properties() {
    return {
      currentView: {type: String}
    };
  }

  constructor() {
    super();
    this.currentView = 'home';
  }

  render() {
    return html`
      <div id="main"></div>
    `;
  }

  connectedCallback() {
    super.connectedCallback();
    document.addEventListener('switch-content', (e) => this._switchContent(e.detail.value));
  }

  firstUpdated(_changedProperties) {
    super.firstUpdated(_changedProperties);
    this._getCurrentViewHtml();
  }

  disconnectedCallback() {
    super.disconnectedCallback();
    this.removeEventListener('switch-content', this._switchContent);
  }

  _getCurrentViewHtml() {
    const main = this.shadowRoot.getElementById('main');
    switch (this.currentView) {
      case 'home':
        return main.innerHTML = homeView.getHTML();
      case 'banking':
        return main.innerHTML = bankingView.getHTML();
      default:
        return main.innerHTML = notFound.getHTML();
    }
  }

  _switchContent(view) {
    if (this.currentView === view) {
      return;
    }
    this.currentView = view;
    this._getCurrentViewHtml();
  }
}

customElements.define('swb-main-content-view', SwbMainContentView);
